package co.com.poli.appconsultorias.repositorios;

import org.springframework.data.repository.CrudRepository;

import co.com.poli.appconsultorias.modelo.entidades.ServicioCo;

public interface ServicioCoRepositorio extends CrudRepository<ServicioCo, Long> {

	public String save(long id);

}
