package co.com.poli.appconsultorias.modelo.entidades;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


	@Entity
	@Table(name = "CONSULTOR")
	public class Consultor {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Long consultor_id;
		private String tipo_consulta_id;
		private String cotizacion_id;
	

		@OneToOne
		private Usuario usuario;
		
		@OneToMany (fetch=FetchType.LAZY, mappedBy="consultas")
	    private Set<ServicioCo> servicio = new HashSet<ServicioCo> (0);

		public Consultor() {
			super();
		}

		/**
		 * @return the consultor_id
		 */
		public Long getConsultor_id() {
			return consultor_id;
		}

		/**
		 * @param consultor_id the consultor_id to set
		 */
		public void setConsultor_id(Long consultor_id) {
			this.consultor_id = consultor_id;
		}

		/**
		 * @return the tipo_consulta_id
		 */
		public String getTipo_consulta_id() {
			return tipo_consulta_id;
		}

		/**
		 * @param tipo_consulta_id the tipo_consulta_id to set
		 */
		public void setTipo_consulta_id(String tipo_consulta_id) {
			this.tipo_consulta_id = tipo_consulta_id;
		}

		/**
		 * @return the cotizacion_id
		 */
		public String getCotizacion_id() {
			return cotizacion_id;
		}

		/**
		 * @param cotizacion_id the cotizacion_id to set
		 */
		public void setCotizacion_id(String cotizacion_id) {
			this.cotizacion_id = cotizacion_id;
		}

		/**
		 * @return the usuario
		 */
		public Usuario getUsuario() {
			return usuario;
		}

		/**
		 * @param usuario the usuario to set
		 */
		public void setUsuario(Usuario usuario) {
			this.usuario = usuario;
		}

		/**
		 * @return the cotizador
		 */
		public Set<ServicioCo> getCotizador() {
			return servicio;
		}

		/**
		 * @param cotizador the cotizador to set
		 */
		public void setCotizador(Set<ServicioCo> cotizador) {
			this.servicio = cotizador;
		}

}
