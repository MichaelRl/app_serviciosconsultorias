package co.com.poli.appconsultorias.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.poli.appconsultorias.modelo.entidades.Usuario;
import co.com.poli.appconsultorias.repositorios.UsuarioRepositorios;

@RestController
public class UserController {

	@Autowired
	private UsuarioRepositorios usuarioRepositorios;

	@RequestMapping(path = "/addUser", method = RequestMethod.POST)
	public @ResponseBody String addNewUser(@RequestParam long usuarioId, @RequestParam String tipo,
			@RequestParam String primerNombre, @RequestParam String edad, @RequestParam String apellido
			,@RequestParam String direccion, @RequestParam String telefono,@RequestParam String correo,
			@RequestParam String username, @RequestParam String password) {

		
		Usuario usuario = new Usuario();
		usuario.setUserId(usuarioId);
		usuario.setTipo(tipo);
        usuario.setPrimerNombre(primerNombre);
        usuario.setApellido(apellido);
        usuario.setDireccion(direccion);
        usuario.setEdad(edad);
        usuario.setTelefono(telefono);
        usuario.setCorreo(correo);
        usuario.setUsername(username);
        usuario.setPassword(password);
		
		usuarioRepositorios.save(usuario);

		return "!usuario guardado!";

	}

}
