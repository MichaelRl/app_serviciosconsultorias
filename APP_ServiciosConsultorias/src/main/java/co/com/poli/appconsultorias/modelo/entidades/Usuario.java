package co.com.poli.appconsultorias.modelo.entidades;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import co.com.poli.appconsultorias.modelo.entidades.Ciudad;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	private String tipo;
	private String primerNombre;
	private String apellido;
	private String direccion;
	private String edad;
	private String telefono;
	private String correo;
	private String username;
	private String password;

	@OneToOne
	private Ciudad ciudadCotizacion;
	
	@OneToMany (fetch=FetchType.LAZY, mappedBy="cotizador")
    private Set<Cotizacion> cotizador = new HashSet<Cotizacion> (0);

	public Usuario() {
		super();
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}



	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the primerNombre
	 */
	public String getPrimerNombre() {
		return primerNombre;
	}

	/**
	 * @param primerNombre the primerNombre to set
	 */
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the edad
	 */
	public String getEdad() {
		return edad;
	}

	/**
	 * @param edad the edad to set
	 */
	public void setEdad(String edad) {
		this.edad = edad;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}



	/**
	 * @return the ciudadCotizacion
	 */
	public Ciudad getCiudadCotizacion() {
		return ciudadCotizacion;
	}

	/**
	 * @param ciudadCotizacion the ciudadCotizacion to set
	 */
	public void setCiudadCotizacion(Ciudad ciudadCotizacion) {
		this.ciudadCotizacion = ciudadCotizacion;
	}

	/**
	 * @return the cotizador
	 */
	public Set<Cotizacion> getCotizador() {
		return cotizador;
	}

	/**
	 * @param cotizador the cotizador to set
	 */
	public void setCotizador(Set<Cotizacion> cotizador) {
		this.cotizador = cotizador;
	}



}
