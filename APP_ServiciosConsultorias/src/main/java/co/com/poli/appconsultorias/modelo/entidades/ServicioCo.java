package co.com.poli.appconsultorias.modelo.entidades;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SERVICIO")
public class ServicioCo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long servicio_id;
	
	@OneToMany (fetch=FetchType.LAZY, mappedBy="cotizacion")
    private Set<Cotizacion> cotizacion = new HashSet<Cotizacion> (0);
	

	private String descripcion_servicio;	

	public ServicioCo() {
		super();
	}

	/**
	 * @return the servicio_id
	 */
	public Long getServicio_id() {
		return servicio_id;
	}

	/**
	 * @param servicio_id the servicio_id to set
	 */
	public void setServicio_id(Long servicio_id) {
		this.servicio_id = servicio_id;
	}

	/**
	 * @return the cotizacion
	 */
	public Set<Cotizacion> getCotizacion() {
		return cotizacion;
	}

	/**
	 * @param cotizacion the cotizacion to set
	 */
	public void setCotizacion(Set<Cotizacion> cotizacion) {
		this.cotizacion = cotizacion;
	}

	/**
	 * @return the descripcion_servicio
	 */
	public String getDescripcion_servicio() {
		return descripcion_servicio;
	}

	/**
	 * @param descripcion_servicio the descripcion_servicio to set
	 */
	public void setDescripcion_servicio(String descripcion_servicio) {
		this.descripcion_servicio = descripcion_servicio;
	}


}
