package co.com.poli.appconsultorias.modelo.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "COTIZACION")
public class Cotizacion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cotizacion_id;

	@ManyToOne
	@JoinColumn(name = "cotizador", nullable = false)
	private Usuario cotizador;
	
	@ManyToOne
	@JoinColumn(name = "cotizacion", nullable = false)
	private ServicioCo cotizacion;		

	private Long descripcionCotizacion;
	private Long productoCotizar;
	private Long valorCotizacion;;

	public Cotizacion() {
		super();
	}


	

	/**
	 * @return the cotizacion_id
	 */
	public Long getCotizacion_id() {
		return cotizacion_id;
	}




	/**
	 * @param cotizacion_id the cotizacion_id to set
	 */
	public void setCotizacion_id(Long cotizacion_id) {
		this.cotizacion_id = cotizacion_id;
	}




	/**
	 * @return the cotizador
	 */
	public Usuario getCotizador() {
		return cotizador;
	}




	/**
	 * @param cotizador the cotizador to set
	 */
	public void setCotizador(Usuario cotizador) {
		this.cotizador = cotizador;
	}




	/**
	 * @return the descripcionCotizacion
	 */
	public Long getDescripcionCotizacion() {
		return descripcionCotizacion;
	}




	/**
	 * @param descripcionCotizacion the descripcionCotizacion to set
	 */
	public void setDescripcionCotizacion(Long descripcionCotizacion) {
		this.descripcionCotizacion = descripcionCotizacion;
	}




	/**
	 * @return the productoCotizar
	 */
	public Long getProductoCotizar() {
		return productoCotizar;
	}




	/**
	 * @param productoCotizar the productoCotizar to set
	 */
	public void setProductoCotizar(Long productoCotizar) {
		this.productoCotizar = productoCotizar;
	}




	/**
	 * @return the valorCotizacion
	 */
	public Long getValorCotizacion() {
		return valorCotizacion;
	}




	/**
	 * @param valorCotizacion the valorCotizacion to set
	 */
	public void setValorCotizacion(Long valorCotizacion) {
		this.valorCotizacion = valorCotizacion;
	}




	/**
	 * @return the cotizacion
	 */
	public ServicioCo getCotizacion() {
		return cotizacion;
	}

	/**
	 * @param cotizacion the cotizacion to set
	 */
	public void setCotizacion(ServicioCo cotizacion) {
		this.cotizacion = cotizacion;
	}			
}
