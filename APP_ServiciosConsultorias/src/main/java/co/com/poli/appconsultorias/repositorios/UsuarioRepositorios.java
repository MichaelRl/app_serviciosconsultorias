package co.com.poli.appconsultorias.repositorios;

import org.springframework.data.repository.CrudRepository;

import co.com.poli.appconsultorias.modelo.entidades.Usuario;

public interface UsuarioRepositorios extends CrudRepository<Usuario, Long> {

	public String save(long id);

}
