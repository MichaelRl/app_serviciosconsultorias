package co.com.poli.appconsultorias.modelo.entidades;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CIUDAD")
public class Ciudad {

	@Id
	private Long ciudad_cotizacion_id;
	private String ciudad_nombre;
	/**
	 * @return the ciudad_cotizacion_id
	 */
	public Long getCiudad_cotizacion_id() {
		return ciudad_cotizacion_id;
	}
	/**
	 * @param ciudad_cotizacion_id the ciudad_cotizacion_id to set
	 */
	public void setCiudad_cotizacion_id(Long ciudad_cotizacion_id) {
		this.ciudad_cotizacion_id = ciudad_cotizacion_id;
	}
	/**
	 * @return the ciudad_nombre
	 */
	public String getCiudad_nombre() {
		return ciudad_nombre;
	}
	/**
	 * @param ciudad_nombre the ciudad_nombre to set
	 */
	public void setCiudad_nombre(String ciudad_nombre) {
		this.ciudad_nombre = ciudad_nombre;
	}
	

	

	
}
