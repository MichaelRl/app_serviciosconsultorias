package co.com.poli.app.consultorias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "co.com.poli.app.consultorias,co.com.poli.appconsultorias.modelo.entidades,co.com.poli.appconsultorias.repositorios,co.com.poli.appconsultorias.controller")
@EntityScan(basePackages = { "co.com.poli.appconsultorias.modelo.entidades" })
@EnableJpaRepositories("co.com.poli.appconsultorias.repositorios")
public class ConsultoriasAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultoriasAppApplication.class, args);
	}

}
